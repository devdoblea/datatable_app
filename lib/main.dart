import 'package:flutter/material.dart';
import 'package:datatable_app/datatable.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DatatableWidget(),
    );
  }
}