import 'package:flutter/material.dart';

class DatatableWidget extends StatefulWidget {
  @override
  _DatatableWidgetState createState() {
    return new _DatatableWidgetState();
  }
}

class _DatatableWidgetState extends State<DatatableWidget> {
  
  Widget bodyData() => DataTable(
    onSelectAll: (b) {},
    sortColumnIndex: 0,
    sortAscending: true,
    columns: <DataColumn>[
    // Aqui van los encabezados
      DataColumn(
        label: Text("Nombre"),
        numeric: false,
        onSort: (i, b) {
          print("$i , $b");
          setState(() {
            nombres.sort((a, b) => a.nombre.compareTo(b.nombre));
          });
        },
        tooltip: "Primer Nombre de la Persona",
      ),
      DataColumn(
        label: Text("Seg. Nombre"),
        numeric: false,
        onSort: (i, b) {
          print("$i , $b");
          setState(() {
            nombres.sort((a, b) => a.snombre.compareTo(b.snombre));
          });
        },
        tooltip: "Segundo Nombre de la Persona",
      ),
      DataColumn(
        label: Text("Apellido"),
        numeric: false,
        onSort: (i, b) {
          print("$i , $b");
          setState(() {
            nombres.sort((a, b) => a.apellido.compareTo(b.apellido));
          });
        },
        tooltip: "Primer Apellido de la Persona",
      ),
      DataColumn(
        label: Text("Seg. Apellido"),
        numeric: false,
        onSort: (i, b) {
          print("$i , $b");
          setState(() {
            nombres.sort((a, b) => a.sapellido.compareTo(b.sapellido));
          });
        },
        tooltip: "Segundo Apellido de la Persona",
      ),
    ],
    //Aqui van los datos que voy a mostrar en forma de lista
    rows: nombres
      .map(
        (nombres) => DataRow(
          //selected: false,
          //onSelectedChanged: (b) {},
          cells: [
            DataCell(
              Text(nombres.nombre),
              showEditIcon: false,
              placeholder: false,
            ),
            DataCell(
              Text(nombres.snombre),
              showEditIcon: false,
              placeholder: false,
            ),
            DataCell(
              Text(nombres.apellido),
              showEditIcon: false,
              placeholder: false,
            ),
            DataCell(
              Text(nombres.sapellido),
              showEditIcon: false,
              placeholder: false,
            ),
          ],
        ),
      ).toList()
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DataTable Flutter'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              child: 
              Text("Lista de Nombres Datatable"),
            ),
            Container(
              alignment: Alignment.topLeft,
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: bodyData(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Nombre {
  String nombre;
  String snombre;
  String apellido;
  String sapellido;

  Nombre({this.nombre, this.snombre, this.apellido, this.sapellido});
}

var nombres = <Nombre>[
  Nombre(nombre: "Rossangel", snombre: "Chiquinquira", apellido: "Antunez", sapellido: "Guerra"),
  Nombre(nombre: "Mariangel", snombre: "Chiquinquira", apellido: "Antunez", sapellido: "Guerra"),
  Nombre(nombre: "Angel", snombre: "Moises", apellido: "Antunez", sapellido: "Valderrey"),
  Nombre(nombre: "Ana", snombre: "Montserrat", apellido: "Rodriguez", sapellido: "Antunez"),
];