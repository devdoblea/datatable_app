Ordenar una tabla haciendo uso del widget Datatable a través del encabezado de la tabla

Aquí la Imagen de como quedó:

![Pantalla Inicial](assets/images/Screenshot_2019-11-24-15-59-56.png)
![Pantalla Inicial](assets/images/Screenshot_2019-11-24-16-00-03.png)

# datatable_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
